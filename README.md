# Bob

Awesome bot - discord and more

## Install

```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
python -m spacy download fr_core_news_md
```

## Install gramallecte


Download grammalecte cli & serveur from https://grammalecte.net/#download

install it with `python3 setup.py install`

