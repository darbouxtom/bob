import spacy
from spacy.tokens import Doc,Token

class Errol():
    """
    Errol analyse les phrase francaises, et essaie de leur trouver un sens, il est assez maladroit.
    Errol doit être capable de:
    - trouver les sujets
    - trouver les verbes associés
    - donner le contexte du verbe (via les auxiliaires, adverbes, negations, etc..)
    - determiner le (les) complement du verbe (cod)
    - analyser les groupes nominaux
    - determiner le contexte global grace aux coi and co
    - corriger les fautes evidentes avec grammalecte
    Errol est basé sur la bibliothèque spacy

    >>> e = Errol()

    >>> e.analyse("Je veux cuisiner un gâteau")._.summary
    [{'subject': Je, 'verb': cuisiner, 'obj': [gâteau], 'sentiment': None}]

    >>> e.analyse("je ne veux pas connaitre la recette du gateau au chocolat")._.summary
    [{'subject': je, 'verb': connaitre, 'obj': [recette], 'sentiment': None}]

    >>> e.analyse("j'ai vraiment envie de manger une crème anglaise")._.summary
    [{'subject': j', 'verb': manger, 'obj': [crème], 'sentiment': None}]

    >>> e.analyse("j'ai vraiment envie de manger un chausson aux pommes")._.summary
    [{'subject': j', 'verb': manger, 'obj': [chausson], 'sentiment': None}]

    >>> e.analyse("j'ai vraiment envie de manger une crème anglaise")._.summary
    [{'subject': j', 'verb': manger, 'obj': [crème], 'sentiment': None}]

    >>> e.analyse("il faut combien d'œufs pour le gâteau au chocolat")._.summary
    [{'subject': il, 'verb': faut, 'obj': [œufs], 'sentiment': None}]

    >>> e.analyse("j'aime manger des pates au roquefort")._.summary
    [{'subject': j', 'verb': manger, 'obj': [pates], 'sentiment': None}]

    >>> e.analyse("Je téléphone à 5 h du matin")._.summary
    [{'subject': Je, 'verb': téléphone, 'obj': [], 'sentiment': None}]

    >>> e.analyse("Je suis à la poursuite de la recette du gateau au chocolat")._.summary
    [{'subject': Je, 'verb': poursuite, 'obj': [recette], 'sentiment': None}]

    >>> e.analyse("ma maman m'a appris une nouvelle recette de pancake hier soir")._.summary
    [{'subject': maman, 'verb': appris, 'obj': [recette], 'sentiment': None}]

    >>> e.analyse("je vais cuisiner un gateau au chocolat")._.summary
    [{'subject': je, 'verb': cuisiner, 'obj': [gateau], 'sentiment': None}]

    >>> e.analyse("Ma copine et moi voudrions savoir comment cuisiner un gateau au chocolat")._.summary
    [{'subject': copine, 'verb': cuisiner, 'obj': [gateau], 'sentiment': None}]

    >>> e.analyse("Je sais comment cuisiner un gateau au chocolat")._.summary
    [{'subject': Je, 'verb': cuisiner, 'obj': [gateau], 'sentiment': None}]

    >>> e.analyse("Elle est ma maman")._.summary
    [{'subject': Elle, 'verb': est, 'obj': [maman], 'sentiment': None}]

    >>> e.analyse("Je ne suis pas certain de vouloir un gateau au chocolat")._.summary
    [{'subject': Je, 'verb': vouloir, 'obj': [gateau], 'sentiment': None}]


    ## To implement
    quelle est la recette du gateau au chocolat de ma maman
    comment prépare t'on une crème anglaise et des croissant ?
    combien coute un gateau au chocolat ?
    c'est le gateau au chocolat dont je voulait la recette hier soir
    doc = nlp("donne moi la recette")



    """
    def __init__(self):
        self.nlp = spacy.load("fr_core_news_md")

        def subjects_from_doc(doc):
            return list(filter(lambda token: token.dep_ == "nsubj", doc))

        def summary_from_doc(doc):
            res = []
            for subject in doc._.subjects:
                verb = subject._.verb
                res.append({
                    'subject': subject,
                    'verb': verb,
                    'obj': verb._.main_obj,
                    'sentiment': None
                })
            return res

        def verb_from_subject(subject):
            potential_action = subject.head
            real_actions = self.filter_children_by_relation(subject.head,["ccomp","xcomp"]) # on va chercher leurs complements si il y'en a
            if real_actions:
                return real_actions[0]
            else:
                return subject.head

        def main_obj_from_verb(verb):
            return self.filter_children_by_relation(verb,["nmod","obj"])

        def previous_verbs_from_verb(verb):
            res = list(sel.filter_children_by_relation(verb,["aux","cop"]))
            if verb.dep_ in ["xcomp","ccomp"]:
                res.append(verb.head._.previous_verbs[0])
            return res

        def compute_negation_from_verb(verb):
            return bool(verb.nbor().text == "pas" or verb.nbor(-1).text == "ne")

        def all_previous_verbs_from_verb(verb):
            previous = verb._.previous_verbs
            if not previous:
                return [verb]
            else:
                return [verb] + previous[0]._.all_previous_verbs

        Doc.set_extension("subjects", getter=subjects_from_doc,force=True)
        Doc.set_extension("summary", getter=summary_from_doc,force=True)

        Token.set_extension("verb", getter=verb_from_subject,force=True)
        Token.set_extension("main_obj", getter=main_obj_from_verb,force=True)
        Token.set_extension("previous_verbs", getter=previous_verbs_from_verb,force=True)
        Token.set_extension("compute_negation", getter=compute_negation_from_verb,force=True)
        Token.set_extension("all_previous_verbs", getter=all_previous_verbs_from_verb,force=True)


    def analyse(self,sentence):
        """
        analyse la phrase passé en paramètres
        """
        # todo add grammalecte
        doc = self.nlp(sentence)
        return doc


    def filter_children_by_relation(self,token,match_relations):
        """
        return all token children which have a relation from <match_relations> with the token
        """
        children = token.children
        matchs = list(filter(lambda token: token.dep_ in match_relations, children))
        return matchs


if __name__ == "__main__":
    import doctest
    doctest.testmod()
