from context import Context

class Cooking(Context):

    def interest(self,sentence):
        if "recette" in sentence:
            return 1
        else:
            return -1

    def analyse(self,sentence,analyse_sentence): # todo set to asynchrone
        return 0

    def answer(self):
        """
        return an answer as a string
        """
        return "j'apprend a cuisiner..."
