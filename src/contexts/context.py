class Context():
    
    def interest(self,sentence):
        """ return a value between 1 and -1 depends of the context interest for the sentence  """
        return -1

    def analyse(self,sentence,analyse_sentence): # todo set to asynchrone
        """
        analyse the mean of the sentence and return a value between -1 and 1 depends of the meaning of the sentence
        analyse_sentence is the nlp analyse of the sentence
        """
        return -1

    def answer(self):
        """
        return an answer as a string
        """
        return "hum... this function is not implemented" 