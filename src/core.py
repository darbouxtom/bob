import spacy
import pkgutil
import inspect
import os
import importlib
import src.contexts


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def import_submodules(package, recursive=True):
    """ Import all submodules of a module, recursively, including subpackages

    :param package: package (name or actual module)
    :type package: str | module
    :rtype: dict[str, types.ModuleType]
    """
    if isinstance(package, str):
        package = importlib.import_module(package)
    results = {}
    for loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        print(name)
        results[full_name] = importlib.import_module(full_name)
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results

class Core():
    def __init__(self):
        list_contexts_class = self.load_contexts(src.contexts)
        self.contexts = self.start_contexts(list_contexts_class)

    def speak(self,sentence):
        max_interest,interested_contexts = self.find_interested_context(sentence)
        if max_interest == -1:
            return self.not_implemented_context()

        analyse_value,final_contexts = self.analyse(interested_contexts,sentence)
        if analyse_value == -1:
            return self.analyse_failed()

        if len(final_contexts) > 1:
            return self.mutiple_context()

        return final_contexts[0].answer()

    def analyse(self,interested_contexts,sentence):
        max_analyse_value = -1
        selected_context = []

        for context_class in interested_contexts:
            local_analyse_value = context_class.analyse(sentence,[])
            if local_analyse_value > max_analyse_value:
                selected_context = [context_class]
                max_analyse_value = local_analyse_value
            elif local_analyse_value == max_analyse_value:
                selected_context.append(context_class)

        return (max_analyse_value,selected_context)

    def find_interested_context(self,sentence):
        max_interest = -1
        selected_context = []
        for context_class in self.contexts:
            local_interest = context_class.interest(sentence)
            if local_interest > max_interest:
                selected_context = [context_class]
                max_interest = local_interest
            elif local_interest == max_interest:
                selected_context.append(context_class)

        return max_interest,selected_context

    def mutiple_context(self):
        return "Sois plus précis !"

    def analyse_failed(self):
        return "je voudrais bien t'aider mais je n'ai pas compris ce que tu voulais"

    def not_implemented_context(self):
        return "Je ne pense pas être competent sur le sujet"

    def start_contexts(self,list_contexts_class):
        list_objects = []
        for context_class in list_contexts_class:
            obj = context_class()
            list_objects.append(obj)

        return list_objects

    def load_contexts(self,package):
        list_contexts = []
        for loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
            module = importlib.import_module(name)
            for name, value in inspect.getmembers(module):
                if name.startswith('__') or name == "Context":
                    continue

                list_contexts.append(value)
        return list_contexts
    """
    def analyse(text):
        nlp = spacy.load("fr_core_news_md")
        doc = nlp(text)

        for token in doc:
            print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
                    token.shape_, token.is_alpha, token.is_stop)
    """
